const http = require("http");
const fs = require('fs');

const host = 'localhost';
const port = 8080;



const requestListener = function (req, res) {
    res.writeHead(200);
    fs.readFile('text.json','utf8', (err, contents) => { 
        if (err) {
            console.error(err)
            return
          }    
        let data = contents;
        console.log(data);
        res.writeHead(200, {'Content-Type': 'application.json'});
        res.write(data);
        res.end();
        
        fs.writeFile('data.txt', data, err => {
        if (err) {
        console.error(err)
        return
        }
        })
    });

};

const server = http.createServer(requestListener);
server.listen(port, host, () => {
    console.log(`Server is running on http://${host}:${port}`);
});